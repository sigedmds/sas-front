export const environment = {
  production: true,
  ambientes: {
    ambiente: {
      apiUrlBase: '/api/'
    },
    seleccionado: 'ambiente'
  },
  debug: false,
  KEY_APP: '5A3374784E424D6F756F4A706C374173',
  token_name: 'sas_token_dev'
};
