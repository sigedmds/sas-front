export const environment = {
  production: false,
  debug: false,
  api: '/api',
  KEY_APP: '5A3374784E424D6F756F4A706C374173',
  token_name: 'sas_token_stg',
  dateVersion:  new Date('8/29/2020 23:00:00'),
  version: '0.0.1',
  headerTitle: ''
};
// FECHA EN INGLES M/D/YYYY
