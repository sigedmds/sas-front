// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  ambientes: {
    ambiente: {
      apiUrlBase: 'https://localhost:44318/'
    },
    seleccionado: 'ambiente'
  },
  debug: true,
  KEY_APP: '5A3374784E424D6F756F4A706C374173',
  token_name: 'sas_token_dev'
};

/*export const environment = {
  production: false,
  debug: true,
  api: '/api',
  KEY_APP: '5A3374784E424D6F756F4A706C374173',
  token_name: 'sas_token_dev',
  dateVersion: new Date(),
  version: '0.0.1',
  headerTitle: ''
};*/

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
