import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { environment } from '../../../environments/environment';
import { UserService } from '../../core/user.service';
import { AuthService } from '../../core/auth.service';
import { User } from '../../shared/models/user.model';
import { Observable } from 'rxjs';
import {ConfirmComponent} from '../../shared/notificaciones/confirm/confirm.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public user: User;
  public isLogged: Observable<boolean>;
  public currentUser: Observable<User>;

  @Output() userLogged: EventEmitter<any> = new EventEmitter();

  constructor(private userService: UserService,
              private authService: AuthService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.user = new User();
    this.user.cuil = '432432432';
    this.user.nombre = 'FEDERICO';
    this.user.apellido = 'SORIA';

    this.isLogged = this.authService.isloggedIn();
    this.currentUser = this.userService.getCurrentUser();
    this.currentUser.subscribe((user) => {
      if (user) {
        if (!this.user) {
          this.user = user;
          this.userLogged.emit(true);
        }
      }
    });
  }

  onLogout() {
    this.userService.redirectToUrl(this.user.cerrarSession);
  }

  changeRepresentado() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {
        title: 'Advertencia',
        content: '¿Está seguro que desea modificar la empresa?',
        closeButtonLabel: 'Cancelar',
        confirmButtonLabel: 'Aceptar'
      }
    });

    dialogRef.afterClosed().subscribe((result: number) => {
      if (!result) { return; }
      this.userService.redirectToUrl(this.user.session);
    });
  }

  goToCidi() {
    this.userService.redirectToUrl('https://cidi.cba.gov.ar');
  }
}
