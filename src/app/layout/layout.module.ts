import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { NavComponent } from './nav/nav.component';
import { HeaderComponent } from './header/header.component';
import { LayoutComponent } from './layout.component';
import { MaterialModule } from '../shared/material.module';

@NgModule({
  declarations: [NavComponent, HeaderComponent, LayoutComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    MaterialModule
  ],
  exports: [NavComponent, HeaderComponent],
})
export class LayoutModule { }
