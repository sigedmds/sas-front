import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../core/user.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
  }

  onUserLogged(isLogged: boolean): void {
    if (!isLogged) {
      this.userService.algo();
    }
  }

}
