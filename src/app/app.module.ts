import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { SharedModule } from './shared/shared.module';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './shared/material.module';
import { IframeGuComponent } from './shared/componentes/modal-iframes-gu/iframe-gu.component';
import { ErrorComponent } from './shared/notificaciones/error/error.component';
import { ConfirmComponent } from './shared/notificaciones/confirm/confirm.component';
import { AlertComponent } from './shared/notificaciones/alert/alert.component';
import { registerLocaleData } from '@angular/common';
import localeArExtra from '@angular/common/locales/extra/es-AR';
import localeAr from '@angular/common/locales/es-AR';

registerLocaleData(localeAr, 'es-AR', localeArExtra);

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {} ;

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    ConfirmComponent,
    ErrorComponent,
    IframeGuComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    AppRoutingModule,
    LayoutModule,
    CoreModule,
    NgxMaskModule.forRoot(options),
    SharedModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-AR' },
    { provide: LOCALE_ID, useValue: 'es-AR' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
