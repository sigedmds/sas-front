export const resources = {
  Comentarios: 'Archivo json con la definición de rutas relativas de los recursos de la api',
  Users: {
    UserById: {
      url: 'users-service/Users/:id'
    },
    GetUsers: {
      url: 'users-service/Users'
    },
    SaveUser: {
      url: 'users-service/Users'
    },
    DeleteUser: {
      url: 'users-service/Users'
    },
  },
};
