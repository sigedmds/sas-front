import { Consulta } from './consulta';

export class PostulanteQuery extends Consulta{
  public cuilEmpresa: string;

  constructor(cuilEmpresa?: string,
              pageNumber?: number,
              sizePage?: number) {
    super(pageNumber,sizePage);
    this.cuilEmpresa = cuilEmpresa;
  }
}
