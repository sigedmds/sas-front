import { Consulta } from './consulta';

export class UsuarioQuery extends Consulta{
  public cuil: string;
  public perfilId: number;
  public institucionId: number;
  public incluirDadoBaja: boolean;

  constructor(cuil?: string,
              perfilId?: number,
              institucionId?: number,
              incluirDadoBaja?: boolean,
              pageNumber?: number,
              sizePage?: number) {
    super(pageNumber, sizePage);
    this.cuil = cuil;
    this.perfilId = perfilId;
    this.institucionId = institucionId;
    this.incluirDadoBaja = incluirDadoBaja;
  }
}
