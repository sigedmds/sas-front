export class EntidadQuery {
  public codigo: string;
  public nombre: string;
  public formaJuridicaId: number;

  constructor(codigo?: string,
              nombre?: string,
              formaJuridicaId?: number){
    this.codigo = codigo;
    this.nombre = nombre;
    this.formaJuridicaId = formaJuridicaId;
  }

}
