export class Consulta {
  public pageNumber: number;
  public pageSize: number;

  constructor(pageNumber?: number,
              pageSize?: number,) {
    this.pageNumber = pageNumber | 0;
    this.pageSize = pageSize | 0;
  }
}
