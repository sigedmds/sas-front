export class DomicilioQuery {
  public nroDocumento: string;
  public codigoPais: string;
  public idSexo: string;
  public idNumero: number;

  constructor(nroDocumento?: string,
              codigoPais?: string,
              idSexo?: string,
              idNumero?: number,){
    this.nroDocumento = nroDocumento;
    this.codigoPais = codigoPais;
    this.idSexo = idSexo;
    this.idNumero = idNumero || 0;
  }

}
