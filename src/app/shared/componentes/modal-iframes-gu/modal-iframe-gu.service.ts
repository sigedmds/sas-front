import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IframeGuComponent, ModalIframeGuData } from './iframe-gu.component';

@Injectable({
  providedIn: 'root'
})
export class ModalIframeGuService {

  constructor(private dialog: MatDialog) {
  }

  openGrupoUnicoModal(iframeContent: string, title: string) {
    return this.dialog.open(IframeGuComponent, {
      width: '1000px',
      height: '700px',
      data: new ModalIframeGuData({
        title: title,
        iframeContent: iframeContent
      })
    });
  }
}

