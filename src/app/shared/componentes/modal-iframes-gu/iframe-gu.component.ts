import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';

export class ModalIframeGuData {
  title: string;
  iframeContent: string;
  confirmButtonLabel: string;
  closeButtonLabel: string;

  constructor(data?) {
    if (data) {
      this.title = data.title;
      this.iframeContent = data.iframeContent;
    }
  }
}
@Component({
  selector: 'app-modal-iframes-gu',
  templateUrl: './iframe-gu.component.html',
  styleUrls: ['./iframe-gu.component.scss']
})
export class IframeGuComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: ModalIframeGuData,
    private _domSanitizer: DomSanitizer
  ) { }

  getUrl(): any{
    return this._domSanitizer.bypassSecurityTrustResourceUrl(this.data.iframeContent);
  }

}

