import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UsuarioCidi } from '../../models/usuario-cidi.model';
import { UsuarioService } from '../../services/usuario.service';
import { CustomValidators } from '../../form-utils/custom-validators';

@Component({
  selector: 'app-busqueda-persona',
  templateUrl: './busqueda-persona.component.html',
  styleUrls: ['./busqueda-persona.component.scss']
})
export class BusquedaPersonaComponent implements OnInit {
  public form: FormGroup;

  @Output() seleccion: EventEmitter<any> = new EventEmitter();
  personas: UsuarioCidi[];

  constructor(private fb: FormBuilder,
              private usuarioService: UsuarioService) {
  }

  ngOnInit() {
    this.personas = [];
    this.form = this.fb.group({
      nroDocumento: new FormControl(),
      codigoPais: new FormControl(),
      idSexo: new FormControl(),
      cuil: new FormControl(undefined, [Validators.required, CustomValidators.cuil])
    });
  }

  buscar(): void {
    this.personas = [];
    this.usuarioService.findUsuario(this.form.get('cuil').value).subscribe(
      (usuario) => {
        if (usuario) {
          this.personas = [usuario];
        }
      }
    );
  }

  seleccionar(persona: UsuarioCidi): void {
    this.seleccion.emit(persona);
  }
}
