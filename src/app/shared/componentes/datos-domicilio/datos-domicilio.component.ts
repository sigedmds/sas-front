import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ComunService } from '../../services/comun.service';
import { DomicilioQuery } from '../../query-models/domicilio.query';
import { ModalIframeGuService } from '../modal-iframes-gu/modal-iframe-gu.service';
import { isEmpty } from '../../form-utils/custom-validators';

@Component({
  selector: 'app-datos-domicilio',
  templateUrl: './datos-domicilio.component.html',
  styleUrls: ['./datos-domicilio.component.scss']
})
export class DatosDomicilioComponent implements OnInit {

  @Output() seleccion: EventEmitter<any> = new EventEmitter();
  @Input() domicilio?: string;
  @Input() nroDocumento: string;
  @Input() codigoPais: string;
  @Input() idSexo: string;
  @Input() idNumero: number;
  @Input() shouldDisable?: boolean;

  constructor(private comunService: ComunService,
              private modalIframeGuService: ModalIframeGuService) { }

  ngOnInit() {
  }

  nuevo() {
    const doc = this.nroDocumento.toString().substr(0, (this.nroDocumento.length <= 8 ? this.nroDocumento.length : 66442233));
    const query = new DomicilioQuery(doc, this.codigoPais, this.idSexo, this.idNumero);
    this.comunService.getDomicilioIframe(query).subscribe(
      (urlIframe) => {
        const modalRef = this.modalIframeGuService.openGrupoUnicoModal(urlIframe, 'Generar docimicio - Grupo Único');
        modalRef.afterClosed().subscribe(() => {
          this.comunService.getDomicilioGenerado(query).subscribe(
            (domicilio) => {
              this.domicilio = domicilio.direccionCompleta;
              this.seleccion.emit(domicilio.idVin);
            });
        });
      }
    );
  }

  deshablitado() {
    return isEmpty(this.nroDocumento) || isEmpty(this.codigoPais) || isEmpty(this.idSexo) || isEmpty(this.idNumero) || this.shouldDisable;
  }
}
