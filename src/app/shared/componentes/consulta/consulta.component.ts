import { Observable, Subject } from 'rxjs';
import { ELEMENTOS, Pagina } from '../../paginacion/pagina-utils';
import { mergeMap, map, pluck, share } from 'rxjs/operators';

export abstract class ConsultaComponent<T> {
  public pagina = new Observable<Pagina<any>>();
  public paginaModificada = new Subject<number>();

  protected constructor() {
  }

  public consultar(pagina?: number): void {
    this.paginaModificada.next(pagina);
  }

  public configurarPaginacion(): void {
    this.pagina = this.paginaModificada.pipe(
      map((numeroPagina) => {
        return {numeroPagina};
      }),
      mergeMap((params: { numeroPagina: number }) => {
        return this.obtenerDatos(params.numeroPagina);
      }),
      share()
    );

    (this.pagina.pipe(pluck(ELEMENTOS)) as Observable<T[]>)
      .subscribe((elementos) => this.elementosBuscados(elementos));
  }

  protected abstract obtenerDatos(numeroPagina: number): Observable<Pagina<T>>;
  protected abstract elementosBuscados(elementos: T[]);
}
