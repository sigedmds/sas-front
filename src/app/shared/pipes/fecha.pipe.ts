import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

const FORMATOS = {
  fullDate: 'fullDate',
  shortDate: 'dd/MM/yyyy'
};

@Pipe({
  name: 'fecha'
})
export class FechaPipe extends DatePipe implements PipeTransform {
  transform(value: any, args?: any, format?: string): any {

    if (value == null) {
      return '-';
    }

    if (args) {
      if(format)
      return super.transform(value, format, null, 'es-AR');
      else
      return super.transform(value, FORMATOS.fullDate, null, 'es-AR');
    } else {
      return super.transform(value, FORMATOS.shortDate, null, 'es-AR');
    }
  }

}
