import { Pipe, PipeTransform } from '@angular/core';
import { SlicePipe } from '@angular/common';

@Pipe({
  name: 'etcetera'
})
export class EtceteraPipe extends SlicePipe implements PipeTransform {

  transform(value: any, end: number = 101): any {
    if (value == null)
      return '';

    end = end < 0 ? 51 : end;
    let result = super.transform(value, 0, end);
    return value.toString().length > end ? result + '...' : result;
  }

}
