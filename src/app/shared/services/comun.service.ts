import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpUtils } from '../utils/http-utils';
import { DomicilioQuery } from '../query-models/domicilio.query';
import { Domicilio } from '../models/domicilio.model';
@Injectable({
  providedIn: 'root'
})
export class ComunService {
  private url = '/shared';

  constructor(private http: HttpClient) { }

  getDomicilioIframe(query: DomicilioQuery): Observable<string> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = { params: HttpUtils.getHttpParams(query), headers};
    return this.http.get<string>(`${this.url}/gu/iframe/domicilio`, options);
  }

  getDomicilioGenerado(query: DomicilioQuery) : Observable<Domicilio>{
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = { params: HttpUtils.getHttpParams(query), headers};
    return this.http.get<Domicilio>(`${this.url}/gu/api/domicilio/generado`, options);
  }
}
