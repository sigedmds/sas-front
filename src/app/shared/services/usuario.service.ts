import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario.model';
import { Pagina } from '../paginacion/pagina-utils';
import { UsuarioQuery } from '../query-models/usuario.query';
import { HttpUtils } from '../utils/http-utils';
import { UsuarioCidi } from '../models/usuario-cidi.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private url = '/usuarios';

  constructor(private http: HttpClient) { }

  public getUsuarios(query: UsuarioQuery): Observable<Pagina<Usuario>> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = { params: HttpUtils.getHttpParams(query), headers };
    return this.http.get<Pagina<Usuario>>(this.url, options);
  }

  public findUsuario(query: string): Observable<UsuarioCidi> {
    return this.http.get<UsuarioCidi>(`${this.url}/ObtenerUsuarioCidi/${query}`);
  }

  registrar(usuario: Usuario): Observable<number> {
    return this.http.post<number>(this.url, usuario);
  }
}
