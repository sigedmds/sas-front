import {
  Component, Input
} from '@angular/core';

@Component({
  selector: 'app-error-message',
  template: `
    <mat-error>
      <strong>{{mensajeError}}</strong>.
    </mat-error>
  `
})
export class ErrorMessageComponent {

  @Input() mensajeError: string;

  constructor() {
  }
}
