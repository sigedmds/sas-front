import {
  Component, Input,
  OnChanges, SimpleChanges
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ErrorMessages } from './custom-validators';

@Component({
  selector: 'app-error-messages',
  templateUrl: './error-messages.html'
})
export class ErrorMessagesComponent implements OnChanges {

  @Input() control: AbstractControl;

  message: string;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.control.currentValue) {
      this.control = changes.control.currentValue;
    }
  }

  getErrorMessage(error: string): string {
    if (this.control.errors.hasOwnProperty(error) && this.control.dirty) {
      return ErrorMessages.messageOf(error, this.control.errors[error]);
    }
    return '';
  }

  /*
    get errorMessage() {
      for (let propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName) && this.control.dirty) {
          return ErrorMessages.messageOf(propertyName, this.control.errors[propertyName]);
        }
      }

      return null;
    }

    public static validarDetalleFormulario(form: FormArray) {
      for (let obj in form.controls) {
        ControlMessagesComponent.validarControls(form.get(obj) as FormGroup);
      }
    }

    public static validarGroup(form: FormGroup) {
      if (form.errors !== null) {
        form.markAsDirty();
        form.updateValueAndValidity();
      }
      document.body.scrollTop = 0;
    }

    public static validarControls(form: FormGroup) {
      for (let obj in form.controls) {
        if (form.get(obj).errors !== null) {
          form.get(obj).markAsDirty();
          form.get(obj).updateValueAndValidity();
        }
      }
      document.body.scrollTop = 0;
    }

    public static validarCampos(campos: FormControl[]): boolean {
      let bool = true;
      for (let campo of campos) {
        if (campo.errors) {
          campo.markAsDirty();
          campo.updateValueAndValidity();
          bool = false;
          document.body.scrollTop = 0;
        }
      }
      return bool;
    }*/
  public getErrors(): string[] {
    return Object.keys(this.control.errors);
  }
}
