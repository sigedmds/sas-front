import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { isNull, isNullOrUndefined, isString } from 'util';

const NUMBER_REGEXP = /^\d+$/;
const EMAIL_REGEXP = /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;
const DECIMAL_REGEXP = /^-?\d+(\.?\d+)?$/;
const DECIMAL_WITH_TWO_DIGITS_REGEXP = /^[0-9]+(.[0-9]{0,2})?$/;
const TEXTO_REGEXP = /[^a-zA-Z]/g;
const TEXTO_SIN_SIMBOLOS_Y_PUNTUACION_REGEXP = /([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)/g;
const FILENAME_REGEXP = /^([a-zA-Z_\-\d])+$/;
const COD_AREA_REGEXP = /^(\d{3,5})$/;
const TELEFONO_REGEXP = /^(\d{7,9})$/;
const CUIL_REGEXP = /^(20|23|24|27|30|33|34)-[0-9]{8}-[0-9]$/;
const FECHA_REGEXP = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

export function isEmpty(value: any): boolean {
  return isNullOrUndefined(value)
    || (isString(value) && value === '');
}

export class CustomValidators {

  public static number(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    return NUMBER_REGEXP.test(control.value) ? null : {number: true};
  }

  public static decimalNumber(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    return DECIMAL_REGEXP.test(control.value) ? null : {number: true};
  }

  public static email(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    return EMAIL_REGEXP.test(control.value) ? null : {email: true};
  }

  public static validText(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    return (control.value.toString().replace(TEXTO_REGEXP, '').length !== 0) ? null : {validText: true};
  }

  public static fileName(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    return FILENAME_REGEXP.test(control.value) ? null : {fileName: true};
  }

  public static minValue(minValue: number): ValidatorFn {

    return (control) => {
      let numero = CustomValidators.number(control);

      if (!isNull(numero)) {
        return numero;
      }

      if (isEmpty(control.value)) {
        return null;
      }

      const value = parseInt(control.value, 10);

      return isNaN(value) || (value < minValue) ?
        {minvalue: {requiredValue: minValue, actualValue: control.value}} :
        null;
    };
  }

  public static maxValue(maxValue: number) {
    return (control) => {

      let numero = CustomValidators.number(control);

      if (!isNull(numero)) {
        return numero;
      }

      if (isEmpty(control.value)) {
        return null;
      }

      const value = parseInt(control.value, 10);

      return isNaN(value) || (value > maxValue) ?
        {maxvalue: {requiredValue: maxValue, actualValue: control.value}} :
        null;
    };
  }

  public static minDecimalValue(minValue: number): ValidatorFn {

    return (control) => {
      let numero = CustomValidators.decimalNumber(control);

      if (!isNull(numero)) {
        return numero;
      }

      if (isEmpty(control.value)) {
        return null;
      }

      const value = parseFloat(control.value);

      return isNaN(value) || (value < minValue) ?
        {minvalue: {requiredValue: minValue, actualValue: control.value}} :
        null;
    };
  }

  public static maxDecimalValue(maxValue: number) {
    return (control) => {

      let numero = CustomValidators.decimalNumber(control);

      if (!isNull(numero)) {
        return numero;
      }

      if (isEmpty(control.value)) {
        return null;
      }

      const value = parseFloat(control.value);

      return isNaN(value) || (value > maxValue) ?
        {maxvalue: {requiredValue: maxValue, actualValue: control.value}} :
        null;
    };
  }

  public static distintoA(valor: any): ValidatorFn {

    return (control) => {

      if (isEmpty(control.value)) {
        return null;
      }

      let valorActual = control.value;

      if (!isNaN(valor)) {
        valorActual = parseFloat(valorActual);
      }

      return (valorActual !== valor) ?
        null :
        {distintoA: {requiredValue: valor, actualValue: control.value}};
    };
  }

  public static codigoArea(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    return COD_AREA_REGEXP.test(control.value) ? null : {codigoArea: true};
  }

  public static telefono(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    return TELEFONO_REGEXP.test(control.value) ? null : {telefono: true};
  }

  public static cuil(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }

    if (control.value.length != 11) {
      return { cuil: true };
    }

    let acumu = 0;
    const digits = control.value.split('');
    const digit = digits.pop();

    for (let i = 0; i < digits.length; i++) {
      acumu += digits[9 - i] * (2 + (i % 6));
    }

    let verif = 11 - (acumu % 11);
    if (verif == 11) {
      verif = 0;
    }
    return digit == verif ? null : { cuil: true };
    //return CUIL_REGEXP.test(control.value) ? null : {cuil: true};
  }

  public static validDate(control: AbstractControl): ValidationErrors | null {
    if (isEmpty(control.value)) {
      return null;
    }
    console.log(FECHA_REGEXP.test(control.value.format('DD/MM/YYYY')));
    return FECHA_REGEXP.test(control.value.format('DD/MM/YYYY')) ? null : { fecha: true };
  }
}

export class ErrorMessages {
  public static messageOf(validatorName: string, validatorValue?: any) {
    let config = {
      validText: 'Debe ingresar un texto valido',
      required: 'El campo es requerido.',
      number: 'Ingresar sólo números.',
      codigoArea: 'Ingresar un código de área válido.',
      telefono: 'Ingresar un teléfono válido.',
      email: 'El email es inválido.',
      cuil: 'El CUIL/CUIT es inválido.',
      fecha: 'La fecha es inválida.',
      maxlength: `No superar los ${validatorValue.requiredLength} caracteres.`,
      minlength: `Ingresar al menos ${validatorValue.requiredLength} caracteres.`,
      minvalue: `El valor mínimo es ${validatorValue.requiredValue}.`,
      maxvalue: `El valor máximo es ${validatorValue.requiredValue}.`,
      minDate: `La fecha mínima es ${validatorValue.requiredValue}.`,
      maxDate: `La fecha máxima es ${validatorValue.requiredValue}.`,
      minDecimalValue: `El valor mínimo es ${validatorValue.requiredValue}.`,
      cantidadDigitos: `La cantidad de digitos requerida es ${validatorValue.requiredValue}.`,
      date: 'Ingresar sólo fechas.',
      alMenosUnCampo: 'Al menos un campo es requerido',
      alMenosUnItem: `Al menos un ${validatorValue.item} es requerido y debe ser marcado como entregado.`,
      soloUnCampo: 'Solo un campo es permitido',
      distintoA: `El valor debe ser distinto a ${validatorValue.requiredValue}.`,
      twodecimal: 'Solo se admiten números positivos y con dos decimales',
      fileName: 'Solo se admiten letras, guiones y números'
    };
    return config[validatorName];
  }
}
