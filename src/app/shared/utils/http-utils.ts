import { isDate } from "util";
import { HttpParameterCodec, HttpParams } from '@angular/common/http';

export interface HttpParamsOptions {
  /**
   * String representation of the HTTP params in URL-query-string format. Mutually exclusive with
   * `fromObject`.
   */
  fromString?: string;

  /** Object map of the HTTP params. Mutually exclusive with `fromString`. */
  fromObject?: {
    [param: string]: string | string[];
  };

  /** Encoding codec used to parse and serialize the params. */
  encoder?: HttpParameterCodec;
}


export class HttpUtils {

  private static getObject(filtros: any, prefijo?: string): object { // prefijo: string = 'consulta'): object {
    if (typeof filtros === 'object') {
      const pref = prefijo ? prefijo + '.' : '';
      const parametros = {};
      Object.getOwnPropertyNames(filtros)
        .map((key: string) => {
          if (filtros[key]) {
            if (isDate(filtros[key])) {
              filtros[key] = filtros[key].toISOString();
            }
            parametros[pref + key] = filtros[key]
          }
        });
      return parametros;
    }
  }

  static getHttpParams(filtros: any, prefijo?: string): HttpParams {
    const httpParams: HttpParamsOptions = { fromObject: this.getObject(filtros) } as HttpParamsOptions;
    return new HttpParams(httpParams);
  }
}
