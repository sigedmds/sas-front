import * as moment from 'moment';

export class Utils {
  private static REGEX_ISO: RegExp = /(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})/;

  static getHoraMinuto(date: Date): string {
    if (date !== null) {
      return date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
    }
  }

  public static isISODate(fecha: string): boolean {
    return (fecha && typeof fecha === 'string' && fecha.match(this.REGEX_ISO) != null);
  }

  static convertToDate(fecha: string): Date {
    let match;
    if (Utils.isISODate(fecha)) {
      match = fecha.match(this.REGEX_ISO);
      const now = new Date();
      const tzo = -now.getTimezoneOffset();
      const dif = tzo >= 0 ? '+' : '-';
      const pad = (num) => {
        const norm = Math.abs(Math.floor(num));
        return (norm < 10 ? '0' : '') + norm;
      };

      let date = match[0];

      if (date.search(/[-+]([0-9][0-9]):([0-9][0-9])/i) === -1) {
        date += dif + pad(tzo / 60) + ':' + pad(tzo % 60);
      }
      const milliseconds = Date.parse(date);
      if (milliseconds !== undefined && milliseconds !== null) {
        return new Date(milliseconds);
      }
    }
  }

  static removeTimeZone(fecha: Date): Date {
    const fec = moment(fecha).utc(true);
    return fec.toDate();
  }
}
