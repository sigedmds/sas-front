import { Injectable } from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import { AuthService } from '../../core/auth.service';
import { Observable, of } from 'rxjs';
import { catchError, concatMap } from 'rxjs/operators';
import { UserService } from '../../core/user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild{

  constructor(private router: Router, private authService: AuthService, private userService: UserService) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isloggedIn().pipe(
        concatMap((isLogged) => !isLogged ? this.authService.signIn() : of(isLogged)),
        catchError((error) => {
          this.router.navigate(['home']);
          return of(false);
      }));
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }
}
