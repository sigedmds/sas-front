import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginacionComponent } from './paginacion/paginacion.component';
import { MaterialModule } from './material.module';
import { MonedaPipe } from './pipes/moneda.pipe';
import { FechaPipe } from './pipes/fecha.pipe';
import { ErrorMessageComponent } from './form-utils/error-message.component';
import { ErrorMessagesComponent } from './form-utils/error-messages.component';
import { DatosDomicilioComponent } from './componentes/datos-domicilio/datos-domicilio.component';
import { BusquedaPersonaComponent } from './componentes/busqueda-persona/busqueda-persona.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { DialogComponent } from './notificaciones/dialog/dialog.component';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { EtceteraPipe } from './pipes/etcetera.pipe';


export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@NgModule({
  declarations: [
    PaginacionComponent,
    MonedaPipe,
    FechaPipe,
    EtceteraPipe,
    ErrorMessageComponent,
    ErrorMessagesComponent,
    DatosDomicilioComponent,
    BusquedaPersonaComponent,
    DialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule
  ],
  exports: [
    PaginacionComponent,
    MonedaPipe,
    FechaPipe,
    EtceteraPipe,
    ErrorMessageComponent,
    ErrorMessagesComponent,
    DatosDomicilioComponent,
    BusquedaPersonaComponent,
    DialogComponent
  ],
  entryComponents: [
    DialogComponent
  ],
  providers: [
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}]
})
export class SharedModule {
}
