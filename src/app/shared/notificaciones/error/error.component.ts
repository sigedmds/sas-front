import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrorModel } from '../../models/error.model';
import { environment } from '../../../../environments/environment';

export class ModalErrorData {
  title: string;
  message: string;
  content: ErrorModel;
  alertType: string;
  closeButtonLabel: string;
  esProd = false;
  prodMessages = '';

  constructor(data?) {
    this.esProd = false; // environment.production;
    if (data) {
      let message = 'Intentelo nuevamente más tarde.';
      if (data.content.errorControlado) {
        message = data.content.errores[0].titulo;
        data.content.errores.splice(0, 1);
      }

      if (this.esProd) {
        for (const error of data.content.errores) {
          this.prodMessages += ' - ' + error.titulo;
        }
      }

      this.title = data.title;
      this.message = message;
      this.content = data.content;
      this.alertType = 'ERROR';
      this.closeButtonLabel = data.closeButtonLabel || 'ACEPTAR';
    }
  }
}

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: ModalErrorData
  ) {
  }

  copy() {

  }
}


