import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmComponent, ModalConfirmData } from './confirm/confirm.component';
import { AlertComponent, AlertType, ModalAlertData } from './alert/alert.component';
import { ErrorModel } from '../models/error.model';
import { ErrorComponent, ModalErrorData } from './error/error.component';

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

  constructor(private dialog: MatDialog) {
  }

  openAlertModal(message: string[], alertType: AlertType, title?: string) {
    return this.dialog.open(AlertComponent, {
      width: '400px',
      panelClass: 'alert-dialog-container',
      data: new ModalAlertData({
        title,
        content: message,
        alertType
      })
    });
  }

  openInfoModal(mensajes: string[], titulo?: string) {
    return this.openAlertModal(mensajes, AlertType.INFO, titulo);
  }

  openWarningModal(mensajes: string[], titulo?: string) {
    return this.openAlertModal(mensajes, AlertType.WARNING, titulo);
  }

  openErrorModal(error: ErrorModel, titulo?: string) {
    return this.dialog.open(ErrorComponent, {
      minWidth: '400px',
      maxWidth: '50vw',
      panelClass: 'alert-dialog-container',
      data: new ModalErrorData({
        title: titulo,
        content: error
      })
    });
  }

  openErrorTextModal(mensaje: string, titulo?: string) {
    return this.dialog.open(ErrorComponent, {
      width: '400px',
      panelClass: 'alert-dialog-container',
      data: new ModalErrorData({
        title: titulo,
        content: mensaje
      })
    });
  }

  openConfirmModal(message: string) {
    return this.dialog.open(ConfirmComponent, {
      data: new ModalConfirmData({
        title: 'Confirmar',
        content: message,
        confirmButtonLabel: 'Aceptar',
        closeButtonLabel: 'Cancelar'
      })
    });
  }
}

