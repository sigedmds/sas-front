import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomValidators} from "../../form-utils/custom-validators";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit{
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() {
    this.form = this.fb.group({
      value: new FormControl('', [Validators.required,
        CustomValidators.number, CustomValidators.minValue(1), CustomValidators.maxValue(this.data.value)])
    });
  }

  onNoClick(): void {
    this.dialogRef.close(0);
  }

  aceptar() {
    this.dialogRef.close(this.form.get('value').value);
  }
}

export interface DialogData {
  title: any;
  description: any;
  value: any;
}
