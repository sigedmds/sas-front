
export class PostulanteModel {
  public rowNum: number;
  public idInscripcion: number;
  public nombre: string;
  public apellido: string;
  public nroDocumento: string;
  public codigoPais: string;
  public cuil: string;
  public sexo: string;
  public fechaNacimiento: Date;
  public codigoAreaCelular: string;
  public celular: string;
  public telefono: string;
  public email: string;
  public domicilio: string;
  public departamento: string;
  public localidad: string;
  public titulo: string;
  public institucionOtorgante: string;
  public fechaEgreso: Date;
  public tieneDiscapacidad: boolean;
  public tipoDiscapacidad: string;
  public nroCertificado: string;

  constructor(idInscripcion?: number,
              rowNum?: number,
              nombre?: string,
              apellido?: string,
              nroDocumento?: string,
              codigoPais?: string,
              sexo?: string,
              cuil?: string,
              fechaNacimiento?: Date,
              codigoAreaCelular?: string,
              celular?: string,
              telefono?: string,
              email?: string,
              domicilio?: string,
              departamento?: string,
              localidad?: string,
              titulo?: string,
              institucionOtorgante?: string,
              fechaEgreso?: Date,
              tieneDiscapacidad?: boolean,
              tipoDiscapacidad?: string,
              nroCertificado?: string) {
    this.idInscripcion = idInscripcion;
    this.rowNum = rowNum;
    this.nombre = nombre;
    this.apellido = apellido;
    this.codigoPais = codigoPais;
    this.sexo = sexo;
    this.cuil = cuil;
    this.nroDocumento = nroDocumento;
    this.fechaNacimiento = fechaNacimiento;
    this.telefono = telefono;
    this.codigoAreaCelular = codigoAreaCelular;
    this.celular = celular;
    this.domicilio = domicilio;
    this.departamento = departamento;
    this.localidad = localidad;
    this.email = email;
    this.titulo = titulo;
    this.institucionOtorgante = institucionOtorgante;
    this.fechaEgreso = fechaEgreso;
    this.tieneDiscapacidad = tieneDiscapacidad;
    this.tipoDiscapacidad = tipoDiscapacidad;
    this.nroCertificado = nroCertificado;
  }
}
