export class Empresa {
  public id: number;
  public cuit: string;
  public codigoActivdad: string;
  public razonSocial: string;
  public codigoArea: string;
  public telefono: string;
  public cantidadPersonal: string;
  public domicilioLegal: string;
  public email: string;
  public codigoAreaReferente: string;
  public telefonoReferente: string;
  public adherido: boolean;
  public calle: string;
  public numero: string;
  public dpto: string;
  public piso: string;
  public departamento: string;
  public localidad: string;

  constructor(id?: number,
              cuit?: string,
              codigoActivdad?: string,
              razonSocial?: string,
              codigoArea?: string,
              telefono?: string,
              cantidadPersonal?: string,
              domicilioLegal?: string,
              email?: string,
              codigoAreaReferente?: string,
              telefonoReferente?: string,
              adherido?: boolean,
              calle?: string,
              numero?: string,
              dpto?: string,
              piso?: string,
              departamento?: string,
              localidad?: string) {
    this.id = id;
    this.cuit = cuit;
    this.codigoActivdad = codigoActivdad;
    this.razonSocial = razonSocial;
    this.codigoArea = codigoArea;
    this.telefono = telefono;
    this.cantidadPersonal = cantidadPersonal;
    this.domicilioLegal = domicilioLegal;
    this.email = email;
    this.codigoAreaReferente = codigoAreaReferente;
    this.telefonoReferente = telefonoReferente;
    this.adherido = adherido;
    this.calle = calle;
    this.numero = numero;
    this.dpto = dpto;
    this.piso = piso;
    this.departamento = departamento;
    this.localidad = localidad;
  }
}
