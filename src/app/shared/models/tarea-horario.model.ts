import {HorarioPracticaModel} from './horario-practica.model';

export class TareaHorarioModel  {
  public idFicha: string;
  public descripcionTarea: string;
  public horarios: HorarioPracticaModel[];
  public cuitEmpresa: string;
  public razonSocial: string;
  public idSede: number;

  constructor(idFicha?: string, descripcionTarea?: string, horarios?: HorarioPracticaModel[],
              cuitEmpresa?: string, razonSocial?: string, idSede?: number) {
    this.idFicha = idFicha;
    this.descripcionTarea = descripcionTarea;
    this.horarios = horarios;
    this.cuitEmpresa = cuitEmpresa;
    this.razonSocial = razonSocial;
    this.idSede = idSede;
  }
}
