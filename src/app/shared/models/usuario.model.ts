
export class Usuario {
  public nombre: string;
  public apellido: string;
  public cuil: string;
  public nroDocumento: string;
  public idNumero: number;
  public idSexo: string;
  public codigoPais: string;

  constructor(nombre?: string,
              apellido?: string,
              cuil?: string,
              nroDocumento?: string,
              idNumero?: number,
              idSexo?: string,
              codigoPais?: string) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.cuil = cuil;
    this.nroDocumento = nroDocumento;
    this.idNumero = idNumero;
    this.idSexo = idSexo;
    this.codigoPais = codigoPais;
  }
}
