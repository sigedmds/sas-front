export class HorarioPracticaModel {
  public idDia: string;
  public dia: string;
  public horaDesde: string;
  public horaHasta: string;

  constructor(idDia?: string,
              dia?: string,
              horaDesde?: string,
              horaHasta?: string) {
    this.idDia = idDia;
    this.dia = dia;
    this.horaDesde = horaDesde;
    this.horaHasta = horaHasta;
  }
}
