export class Parametro {
  public id: number;
  public valor: string;

  constructor(id?: number,
              valor?: string) {
    this.id = id;
    this.valor = valor;
  }
}
