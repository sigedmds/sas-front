import {PostulanteModel} from './postulante.model';

export class BeneficiarioModel extends PostulanteModel {
  public idFicha: string;
  public idEstado: number;
  public estado: string;
  public fechaInicio: Date;
  public cantidadHs: string;

  constructor(idFicha?: string, idEstado?: number, estado?: string, cantidadHs?: string, fechaInicio?: Date) {
    super();
    this.idFicha = idFicha;
    this.idEstado = idEstado;
    this.estado = estado;
    this.cantidadHs = cantidadHs;
    this.fechaInicio = fechaInicio;
  }
}
