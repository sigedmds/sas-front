import { MensajeError } from './mensaje-error.model';

export class ErrorModel {
  public codigo: number;
  public errorControlado: boolean;
  public errores: MensajeError[];

  constructor(codigo?: number,
              errorControlado?: boolean,
              errores?: MensajeError[]){
    this.codigo = codigo;
    this.errorControlado = errorControlado;
    this.errores = errores;
  }

}
