export class Persona {
  public apellido: string;
  public nombre: string;
  public nroDocumento: string;
  public idNumero: number;
  public idSexo: string;
  public codigoPais: string;

  constructor(apellido?: string,
              nombre?: string,
              nroDocumento?: string,
              idNumero?: number,
              idSexo?: string,
              codigoPais?: string) {
    this.apellido = apellido;
    this.nombre = nombre;
    this.nroDocumento = nroDocumento;
    this.idNumero = idNumero;
    this.idSexo = idSexo;
    this.codigoPais = codigoPais;
  }
}
