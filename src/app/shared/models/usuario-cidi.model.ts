import {Empresa} from './empresa.model';

export class UsuarioCidi {
  public cuil: string;
  public nroDocumento: string;
  public apellido: string;
  public nombre: string;
  public fechaNacimiento: string;
  public idSexo: string;
  public codigoPais: string;
  public idNumero: string;
  public email: string;
  public codigoArea: string;
  public telefono: string;
  public empleado: string;
  public empleadoId: string;
  public empresaRepresentada: Empresa;

  constructor(cuil?: string,
              nroDocumento?: string,
              apellido?: string,
              nombre?: string,
              fechaNacimiento?: string,
              idSexo?: string,
              codigoPais?: string,
              idNumero?: string,
              email?: string,
              codigoArea?: string,
              telefono?: string,
              empleado?: string,
              empleadoId?: string,
              empresaRepresentada?: Empresa){
  this.cuil = cuil;
  this.nroDocumento = nroDocumento;
  this.apellido = apellido;
  this.nombre = nombre;
  this.fechaNacimiento = fechaNacimiento;
  this.idSexo = idSexo;
  this.codigoPais = codigoPais;
  this.idNumero = idNumero;
  this.email = email;
  this.codigoArea = codigoArea;
  this.telefono = telefono;
  this.empleado = empleado;
  this.empleadoId = empleadoId;
  this.empresaRepresentada = empresaRepresentada;
  }
}
