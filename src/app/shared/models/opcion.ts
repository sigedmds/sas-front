export class Opcion {
  public clave: number | any;
  public valor: string;

  constructor(clave?: number | any,
              valor?: string) {
    this.clave = clave;
    this.valor = valor;
  }
}
