export class Domicilio {
  public idVin: number;
  public direccionCompleta: string;

  constructor(idVin?: number, direccionCompleta?: string) {
    this.idVin = idVin;
    this.direccionCompleta = direccionCompleta;
  }
}
