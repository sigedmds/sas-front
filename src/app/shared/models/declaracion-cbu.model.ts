export class DeclaracionCbuModel {
  public idEmpresa: number;
  public cuit: string;
  public idBanco: number;
  public numeroCuenta: string;
  public idTipoCuenta: number; // 1-caja ahorro, 2-cta corriente
  public cbu: string;

  constructor(idEmpresa?: number,
              cuit?: string,
              idBanco?: number,
              numeroCuenta?: string,
              cbu?: string,
              idTipoCuenta?: number) {
    this.idEmpresa = idEmpresa;
    this.cuit = cuit;
    this.idBanco = idBanco;
    this.numeroCuenta = numeroCuenta;
    this.cbu = cbu;
    this.idTipoCuenta = idTipoCuenta;
  }
}
