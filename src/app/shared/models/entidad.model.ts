export class EntidadModel {
  public codigo: string;
  public nombre: string;
  public formaJuridica: string;
  public fechaAlta: Date;
  public fechaVencimiento: Date;
  public nroInscripcion: string;

  constructor(codigo?: string,
              nombre?: string,
              formaJuridica?: string,
              fechaAlta?: Date,
              fechaVencimiento?: Date,
              nroInscripcion?: string) {
    this.codigo = codigo;
    this.nombre = nombre;
    this.formaJuridica = formaJuridica;
    this.fechaAlta = fechaAlta;
    this.fechaVencimiento = fechaVencimiento;
    this.nroInscripcion = nroInscripcion;
  }
}
