import {Empresa} from './empresa.model';

export class User {
  public id: number;
  public nombre: string;
  public apellido: string;
  public cuil: string;
  public cuit: string;
  public nroDocumento: string;
  public sexo: string;
  public session: string;
  public cerrarSession: string;
  public nombreRepresentado: string;
  public actualizarEmpresa: boolean;
  public tieneRepresentados: string;
  public esBeneficiario: boolean;
  public habilitarCambio: boolean;
  public goToUrl: string;
  public empresa: Empresa;
  public requerirNivel2: boolean;
  public esActivo: boolean;
  public programa: string;
  public fechaFin: Date;

constructor(id?: number,
            nombre?: string,
            apellido?: string,
            nroDocumento?: string,
            cuil?: string,
            cuit?: string,
            sexo?: string,
            session?: string,
            cerrarSession?: string,
            nombreRepresentado?: string,
            actualizarEmpresa?: boolean,
            esBeneficiario?: boolean,
            habilitarCambio?: boolean,
            tieneRepresentados?: string,
            goToUrl?: string,
            empresa?: Empresa,
            requerirNivel2?: boolean,
            esActivo?: boolean,
            programa?: string,
            fechaFin?: Date) {
    this.id = id;
    this.nombre = nombre;
    this.apellido = apellido;
    this.nroDocumento = nroDocumento;
    this.cuil = cuil;
    this.cuit = cuit;
    this.nombreRepresentado = nombreRepresentado;
    this.actualizarEmpresa = actualizarEmpresa;
    this.session = session;
    this.cerrarSession = cerrarSession;
    this.sexo = sexo;
    this.tieneRepresentados = tieneRepresentados;
    this.esBeneficiario = esBeneficiario;
    this.habilitarCambio = habilitarCambio;
    this.goToUrl = goToUrl;
    this.empresa = empresa;
    this.requerirNivel2 = requerirNivel2;
    this.esActivo = esActivo;
    this.programa = programa;
    this.fechaFin = fechaFin;
  }
}
