export class Permission {
  id: number;
  name: string;
  url: string;
  icon: string;
  isNavItem: boolean;

  constructor(id?: number, name?: string, url?: string, icon?: string, isNavItem?: boolean) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.icon = icon;
    this.isNavItem = isNavItem;
  }
}
