export class Sede {
  public id: number;
  public apellido: string;
  public nombre: string;
  public telefono: string;
  public email: string;
  public codigoPostal: string;
  public idLocalidad: number;
  public localidad: string;
  public calle: string;
  public numero: string;
  public idEmpresa: number;
  public domicilio: string;

  constructor(id?: number, apellido?: string, nombre?: string, telefono?: string, email?: string, codigoPostal?: string,
              idLocalidad?: number, localidad?: string, calle?: string, numero?: string, idEmpresa?: number, domicilio?: string) {
    this.id = id;
    this.apellido = apellido;
    this.nombre = nombre;
    this.telefono = telefono;
    this.email = email;
    this.codigoPostal = codigoPostal;
    this.idLocalidad = idLocalidad;
    this.localidad = localidad;
    this.calle = calle;
    this.numero = numero;
    this.idEmpresa = idEmpresa;
    this.domicilio = domicilio;
  }
}
