import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, map, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = '/autenticacion/token';
  private grantType = 'cidi';
  private usuarioInicioSesion = new BehaviorSubject<boolean>(this.hasToken());

  constructor(private router: Router,
              private http: HttpClient) { }

  public signIn(): Observable<boolean> {
    const urlEncoded = `grant_type=${this.grantType}&client_id=${environment.KEY_APP}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };
    return this.http.post<Token>(this.url, urlEncoded, httpOptions)
      .pipe(
        map((token) => {
          localStorage.setItem(environment.token_name, token.access_token);
          this.usuarioInicioSesion.next(true);
          return true;
        }),
        catchError((res: Response) => {
          localStorage.setItem('login', res.headers.get('x-auth-login-path'));
          window.location.href = res.headers.get('x-auth-login-path') || '/home';
          return throwError(res);
        })
      );
  }

  private hasToken(): boolean {
    return !!this.token();
  }

  public token(): string {
    return localStorage.getItem(environment.token_name);
  }

  public isloggedIn(): Observable<boolean> {
    return this.usuarioInicioSesion.asObservable()
      .pipe(
        share()
      );
  }

  public signOut(): void {
    localStorage.removeItem(environment.token_name);
  }
}

export class Token {
  public access_token: string;

  constructor(accessToken?: string) {
    this.access_token = accessToken;
  }
}
