import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { User } from '../shared/models/user.model';
import { Permission } from '../shared/models/permission';
import { HttpClient } from '@angular/common/http';
import { catchError, map, share } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = '/usuarios';
  public loggedUser = new BehaviorSubject<User>(null);
  public permissions = new BehaviorSubject<Permission[]>([]);

  constructor(private http: HttpClient,
              private autService: AuthService) {
    /*
    autService.isloggedIn().subscribe((isLogged) => {
      if (isLogged) {
        this.getUser().subscribe((user) => {
          this.loggedUser.next(user);
        });
      } else {
        this.loggedUser.next(null);
        this.permissions.next([]);
      }
    });
     */
  }

  public algo() {
    this.autService.isloggedIn().subscribe((isLogged) => {
      if (isLogged) {
        this.getUser().subscribe((user) => {
          this.loggedUser.next(user);
        });
      } else {
        this.loggedUser.next(null);
        this.permissions.next([]);
        this.autService.signIn().subscribe();
      }
    });
  }

  public getCurrentUser(): Observable<User> {
    return this.loggedUser
      .asObservable()
      .pipe(
        share()
      );
  }

  public hasPermission(url: string): boolean {
    return this.permissions.value.some((permiso) => {
      return permiso.url === url;
    });
  }

  public getPermissions(): Observable<Permission[]> {
    return this.http
      .get<Permission[]>(this.url + '/yo/permisos')
      .pipe(
        catchError((res: Response) => {
          const href = res.headers.get('x-auth-login-path') || '/';
          window.location.href = href;
          return throwError(res);
        })
      );
  }

  private getUser(): Observable<User> {
    return this.http.get<User>(`${this.url}/yo`)
      .pipe(
        map((user: User) => {
          if (user.actualizarEmpresa) {
            this.redirectToUrl(user.session);
          }
          return user;
        }),
        catchError((res: Response) => {
          window.location.href = res.headers.get('x-auth-login-path') || '/';
          return throwError(res);
        })
      );
  }

  public getUserPipe(): Observable<User> {
    return this.getUser().pipe(
      map(user => {
        this.loggedUser.next(user);
        return user;
      }),
      share());
  }

  public redirectToUrl(url: string): void {
    this.autService.signOut();
    localStorage.clear();
    window.location.href = url;
  }

  public realizoCambioEmpresa(realizo: boolean): void {
    const user = this.loggedUser.value;
    user.habilitarCambio = !realizo;
    this.loggedUser.next(user);
  }
}
