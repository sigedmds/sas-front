export class Respuesta<T> {
  codigo: number;
  errorControlado: boolean;
  resultado: T;
  errores: object;

  constructor(codigo?: number,
              errorControlado?: boolean,
              resultado?: T,
              errores?: object) {
    this.codigo = codigo;
    this.errorControlado = errorControlado;
    this.resultado = resultado;
    this.errores = errores;
  }
}
