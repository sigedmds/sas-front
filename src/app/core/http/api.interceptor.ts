import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, filter, finalize, map, switchMap, take } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthService } from '../auth.service';
import { SpinnerService } from '../loading-spinner/spinner.service';
import { Respuesta } from './respuesta-model';
import { NotificacionService } from '../../shared/notificaciones/notificacion.service';
import { Utils } from '../../shared/utils/utils';
import { isMoment } from 'moment';

@Injectable()
export class APIInterceptor implements HttpInterceptor {
  private requestCount: number;

  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor(private spinnerService: SpinnerService,
              private notificacionService: NotificacionService,
              public auth: AuthService) {
    this.requestCount = 0;
  }

  static updateUrl(req: string): string {
    return environment.ambientes.ambiente.apiUrlBase + req;
  }

  static convertDateFromBody(body: any): any {
    if (body === null || body === undefined || typeof body !== 'object') {
      return body;
    }
    if (isMoment(body)) {
      body.utc(true);
      return body;
    }
    for (const key of Object.keys(body)) {
      const value = body[key];
      if (value instanceof Date) {
        body[key] = Utils.removeTimeZone(value);
      } else if (typeof value === 'object') {
        this.convertDateFromBody(value);
      }
    }
  }

  private addToken(request: HttpRequest<any>): any {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.token()}`
      },
      url: APIInterceptor.updateUrl(request.url)
    });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<Respuesta<any>>> {
    this.requestCount++;
    this.spinnerService.show();

    request = this.getFormattedBody(request);

    if ((this.auth.token())) {
      return next.handle(this.addToken(request))
        .pipe(
/*          map((response) => {
            if (response instanceof HttpResponse) {
              return response.clone({body: response.body.resultado === undefined ? null : response.body.resultado});
            }
          }),*/
          map((response) => {
              if (response instanceof HttpResponse) {
                return response.clone({body: this.convertToDate(response.body)});
              }
            }
          ),
          catchError((response) => {
            if (response.status === 401 || response.status === 403) {
              this.auth.signOut();
              return this.handle401Error(request, next);
            } else {
              const errorMessage = JSON.parse(JSON.stringify(response));
              this.notificacionService.openErrorModal(errorMessage.error, 'Algo ha ocurrido' );
              return throwError(response);
            }
          }),
          finalize(() => this.removeRequest())
        );
    } else {
      request = request.clone({
        url: APIInterceptor.updateUrl(request.url)
      });
      return next.handle(request)
        .pipe(
          map((response) => {
            return response;
          }),
          catchError((response) => {
            const errorMessage = JSON.parse(JSON.stringify(response));
            console.error(errorMessage.error);
            return throwError(response);
          }),
          finalize(() => this.removeRequest())
        );
    }
  }

  private getFormattedBody(request: HttpRequest<any>): HttpRequest<any> {
    return request.clone({
      body: APIInterceptor.convertDateFromBody(request.body)
    });
  }

  private removeRequest(): void{
    if (this.requestCount > 0) {
      this.requestCount--;
      this.updateSpinner();
    }
  }

  private updateSpinner(): void {
    if (this.requestCount === 0) {
      this.spinnerService.hide();
    }
  }

  private convertToDate(body: any): any {
    if (body === null || body === undefined || typeof body !== 'object') {
      return body;
    }
    for (const key of Object.keys(body)) {
      const value = body[key];
      if (Utils.isISODate(value)) {
        body[key] = new Date(value);
      } else if (typeof value === 'object') {
        this.convertToDate(value);
      }
    }
  }

  handle401Error(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<Respuesta<any>>> {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;

      this.auth.signOut();
      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);

      return this.auth.signIn().pipe(
        switchMap((newToken: boolean) => {
          if (newToken) {
            this.tokenSubject.next(newToken);
            return next.handle(this.addToken(req));
          }
        }),
        catchError(error => {
          // If there is an exception calling 'refreshToken', bad news so logout.
          return throwError(error);
        }),
        finalize(() => {
          this.isRefreshingToken = false;
        }));
    } else {
      return this.tokenSubject.pipe(
        filter(token => token),
        take(1),
        switchMap(() => {
          return next.handle(this.addToken(req));
        }));
    }
  }
}
