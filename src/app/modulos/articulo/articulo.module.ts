import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegistrarArticuloComponent} from './registrar-articulo/registrar-articulo.component';
import {ArticuloRoutingModule} from './articulo-routing.module';
import {MaterialModule} from '../../shared/material.module';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [RegistrarArticuloComponent],
  imports: [
    CommonModule,
    ArticuloRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [],
  providers: [
    {
      provide: MAT_DATE_LOCALE, useValue: 'es-AR'
    },
  ],
  entryComponents: []
})
export class ArticuloModule {
}
