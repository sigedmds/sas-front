import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegistrarArticuloComponent} from './registrar-articulo/registrar-articulo.component';

const routes: Routes = [
  {
    path: 'articulo', children: [
      //{path: '', component: },
      {path: 'registrar', component: RegistrarArticuloComponent},
      //{path: 'actualizar/:id', component: },
      //{path: 'baja/:id', component: },
      //{path: 'ver/:id', component: },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticuloRoutingModule {
}
