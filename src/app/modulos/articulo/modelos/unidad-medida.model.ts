export class UnidadMedidaModel {
  public id: number;
  public nombre: string;

  public constructor(id?: number,
                     nombre?: string) {
    this.id = id;
    this.nombre = nombre;
  }
}
