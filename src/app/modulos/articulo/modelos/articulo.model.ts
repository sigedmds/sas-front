import {UnidadMedidaModel} from "./unidad-medida.model";

export class ArticuloModel {
  public id: number;
  public nombre: string;
  public fechaAlta: Date;
  public unidadMedida: UnidadMedidaModel;

  public constructor(id?: number,
                     nombre?: string,
                     fechaAlta?: Date,
                     unidadMedida?: UnidadMedidaModel) {
    this.id = id;
    this.nombre = nombre;
    this.fechaAlta = fechaAlta;
    this.unidadMedida = unidadMedida ? unidadMedida : new UnidadMedidaModel();
  }
}
