import {Component, OnInit} from '@angular/core';
import {ArticuloModel} from '../modelos/articulo.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UnidadMedidaModel} from '../modelos/unidad-medida.model';
import {NotificacionService} from '../../../shared/notificaciones/notificacion.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-registrar-articulo',
  templateUrl: './registrar-articulo.component.html',
  styleUrls: ['./registrar-articulo.component.scss']
})
export class RegistrarArticuloComponent implements OnInit {

  public form: FormGroup;
  public articulo = new ArticuloModel();
  public lsUnidades: UnidadMedidaModel[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private notificacionService: NotificacionService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    // TODO quitar esto cuando este el servicio de unidades de medida
    this.lsUnidades.push(new UnidadMedidaModel(1, 'Metros'));
    this.lsUnidades.push(new UnidadMedidaModel(2, 'Litros'));
    this.lsUnidades.push(new UnidadMedidaModel(3, 'Kilogramos'));


    this.articulo.fechaAlta = new Date();
    this.crearFormulario();
  }

  public crearFormulario(): void {
    this.form = this.formBuilder.group({
      txNombre: [null, [Validators.required]],
      nUnidadMedida: [-1, [Validators.required]]
    });
  }

  public prepararComando() {
    this.articulo.nombre = this.txNombre.value;
    this.articulo.unidadMedida.id = this.nUnidadMedida.value;
  }

  public registrarArticulo() {
    this.prepararComando();
    this.notificacionService.openConfirmModal('Se creará un nuevo artículo, ¿Desea continuar?').afterClosed().subscribe((res) => {
      if (res) {
        this.snackBar.open('Se registró con éxito.', 'Cerrar', {
          duration: 5000, panelClass: ['snackbar-success']
        });
        this.crearFormulario();
      }
      else {
        console.error('se canceló');
      }
    });
  }

  public volver() {
    this.router.navigate(['']);
  }
  get txNombre() {
    return this.form.get('txNombre');
  }

  get nUnidadMedida() {
    return this.form.get('nUnidadMedida');
  }


}
