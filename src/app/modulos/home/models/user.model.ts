import {RoleModel} from './role.model';

export class UserModel {
  public id: number;
  public name: string;
  public role: RoleModel;
  public username: string;

  public constructor(id?: number,
                     name?: string,
                     role?: RoleModel,
                     username?: string) {
    this.id = id;
    this.name = name;
    this.role = role;
    this.username = username;
  }
}
