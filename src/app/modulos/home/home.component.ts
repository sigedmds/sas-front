import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../services/user.service';
import {UserModel} from './models/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [UsersService]
})
export class HomeComponent implements OnInit {
  lsUsers: UserModel[] = [];

  constructor(
    private usersService: UsersService 
  ) {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
/*    this.usersService.getUsers().subscribe((usersList) => {
      this.lsUsers = usersList;
      console.error(this.lsUsers);
    });*/
  }
}
