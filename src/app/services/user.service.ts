import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ApiService} from '../http/api.service';
import {HttpClient} from '@angular/common/http';
import {resources} from '../configs/api-resources.config';
import {UserModel} from '../modulos/home/models/user.model';

@Injectable()
export class UsersService extends ApiService {

  constructor(http: HttpClient) {
    super(http);
  }

  public getUsers(): Observable<UserModel[]> {
    return this.GetResource<UserModel[]>(resources.Users.GetUsers.url);
  }
}
