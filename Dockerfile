FROM node:12 as build
WORKDIR /sas-front
RUN npm install -g @angular/cli

COPY ./Web/package.json ./package.json
RUN npm i

COPY ./Web .
COPY ./Web/configs/ .
RUN ng build --prod
RUN mkdir generado
RUN cp -r dist/*/* generado/

FROM nginx:1.13.12-alpine
COPY --from=build /sas-front/generado /usr/share/nginx/html
COPY /Web/configs/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]
